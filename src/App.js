import React from "react";
import './App.css';
import Header from './Components/Header';
import CounterAndData from "./Components/CounterAndData";
import Footer from "./Components/Footer";

function App() {
  return (
    <div className="App">
        <Header />
        <CounterAndData />
        <Footer />
    </div>
  );
}

export default App;
