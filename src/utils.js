/* eslint-disable use-isnan */
const generateText = (name, age) => {
    return `${name} (${age} years old)`;
}
  
const validateInput = (text, notEmpty, isNumber) => {
    // Validate user input with two pre-defined rules
    if (!text) {
      return false;
    }
    if (notEmpty && text.trim().length === 0) {
      return false;
    }
    if (isNumber && +text === NaN) {
      return false;
    }
    return true;
};

const sum = (num1, num2) => {
    return num1 + num2;
}

module.exports = { generateText, validateInput, sum };
