import React, { useState, useEffect, useReducer } from "react";
import { generateText } from '../utils';
import axios from 'axios';

export const dataReducer = (state, action) => {
    if (action.type === 'SET_ERROR') {
      return { ...state, list: [], error: true };
    }
   
    if (action.type === 'SET_LIST') {
      return { ...state, list: action.list, error: null };
    }
   
    throw new Error();
  };
   
  const initialData = {
    list: [],
    error: null,
  };
  
const CounterAndData = () => {
    const [counter, setCounter] = useState(0);
    const [name, setName ] = useState('');
    const [age, setAge] = useState('');
    const [user, setUser] = useState('');
    const [data, dispatch] = useReducer(dataReducer, initialData);
  
    const addUser = () => {
      setUser(generateText(name, age));
    }
    
    useEffect(() => {
      // API call to search Hacker News website
      axios
        .get('http://hn.algolia.com/api/v1/search?query=react')
        .then(response => {
          dispatch({ type: 'SET_LIST', list: response.data.hits });
        })
        .catch(() => {
          dispatch({ type: 'SET_ERROR' });
        });
    }, []);
   
    return (
      <div>
        <section>
          <div className="control-panel hidden">
            <div className="input-container">
                <label htmlFor="name">Name</label>
                <input type="text" id="name" value={name} onChange={e => setName(e.target.value)}/>
            </div>
            <div className="input-container">
                <label htmlFor="age">Age</label>
                <input type="number" id="age" value={age} onChange={e => setAge(e.target.value)}/>
            </div>
            <button id="btnAddUser" className="button" onClick={addUser}>Add User</button>
            <p>{user}</p>
          </div>
        </section>
        <section>
          <h1>My Counter</h1>
          <p id="my-counter">{counter}</p>

          <button id="increment" type="button" onClick={() => setCounter(counter + 1)}>
            Increment
          </button>
    
          <button id="decrement" type="button" onClick={() => setCounter(counter - 1)}>
            Decrement
          </button>
        </section>
        <section>
          <h2>My Async Data - Called once on load</h2>
          {data.error && <div className="error">Error</div>}
          <ul>
            {data.list.map(item => (
              <li key={item.objectID}>{item.title}</li>
            ))}
          </ul>
        </section>
      </div>
    );
  };
   
  export default CounterAndData;