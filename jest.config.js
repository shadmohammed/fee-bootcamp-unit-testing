module.exports = {
    // The pattern or patterns Jest uses to detect test files
    // Default it looks for .js, .jsx, .ts and .tsx files inside of __tests__ folders, 
    // as well as any files with a suffix of .test or .spec (e.g. Component.test.js or Component.spec.js). 
    // It will also find files called test.js or spec.js
    testRegex: "((\\.|/*.)(spec|test))\\.js?$",
    
    // These are the extensions Jest will look for left-to-right order
    // Default: ["js", "jsx", "ts", "tsx", "json", "node"]
    moduleFileExtensions: ['jsx', 'js'],  
    
    // A list of paths to modules that run some code to configure or set up the testing environment.
    // Example any setup for Enzyme or React Testing Library that is required before running the tests
    setupFiles: ['<rootDir>/jest.setup.js'],

    // An array of regexp pattern strings that are matched against all test paths before executing the test. 
    // If the test path matches any of the patterns, it will be skipped.
    // testPathIgnorePatterns: ['/some-path/tests/'],

    // An array of regexp pattern strings that are matched against all file paths before executing the test. 
    // If the file path matches any of the patterns, coverage information will be skipped.
    // coveragePathIgnorePatterns: ['/node_modules/', 'enzyme.js'],
  };
  